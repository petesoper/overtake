#!/usr/bin/env python3

import argparse, datetime, sys

NAME_AND_VERSION="overtake 1.10"

# From the command line use a name, (higher) total, and (lower) estimated daily increase and a 
# second name, (lower) total and (higher) estimated daily increase. 
# Calculate the days and hours before the second entity will surpass the first.

# Usage: overtake <first label> <higher case total #> <lower rate#> <second label> <lower case total #> <higher rate#>
# Example:
#     overtake Indiana 36997 400 'North Carolina' 34718 1000

# Example output:
#  Indiana total cases: 36997
#  Indiana rate: 400
#  North Carolina total cases: 34718
#  North Carolina rate: 1000
#  North Carolina will overtake Indiana in     3 days    20 hours

# Peter Soper (pete@soper.us) 2020 MIT License
# June, 2020

def usage(msg):
    print(msg)
    print("Usage: overtake <first label> <higher case total #> <lower rate#> <second label> <lower case total #> <higher rate#>")
    quit(1)

if __name__ == "__main__":
    print("{0:s} {1:s}".format(NAME_AND_VERSION, str(datetime.date.today())))
    if len(sys.argv) < 7:
        usage("too many or too few arguments")
    
    try:
        first_label = sys.argv[1]
        first_total = int(sys.argv[2])
        first_rate = int(sys.argv[3])
        second_label = sys.argv[4]
        second_total = int(sys.argv[5])
        second_rate = int(sys.argv[6])
    except:
        usage("error casting argument to integer")


    # announce input

    print("{0:s} total cases: {1:d}".format(first_label, first_total))
    print("{0:s} rate: {1:d}".format(first_label, first_rate))
    print("{0:s} total cases: {1:d}".format(second_label, 
       second_total))
    print("{0:s} rate: {1:d}".format(second_label, second_rate))

    if first_total <= second_total:
        usage("first total not greater than second")

    if second_rate <= first_rate:
        usage("second rate not greater than first")


    # step time one hour at a time until

    delta_hours = 0;

    while second_total <= first_total:
        first_total += (first_rate / 24)
        second_total += (second_rate / 24)
        delta_hours += 1

    days = int(delta_hours / 24)
    if days > 0:
      delta_hours = delta_hours % 24
      print("{0:s} will overtake {1:s} in {2:d} days {3:d} hours".format(second_label, first_label,days, delta_hours))
    else:
      print("{0:s} will overtake {1:s} in {3:d} hours".format(second_label, first_label, delta_hours))
